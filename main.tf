terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

provider "docker" {}

resource "docker_image" "gitlab" {
  name = "gitlab/gitlab-ce:15.4.6-ce.0"
}
locals {
    path_volume = [
    {path = "/etc/gitlab", name="gitlab_config"}, 
    {path = "/var/log/gitlab", name="gitlab_logs"},
    {path = "/var/opt/gitlab", name="gitlab_data"}]
    ports = ["22", "443"]
}

resource "docker_container" "gitlab" {
  image    = docker_image.gitlab.name
  name     = "gitlab_repo"
  attach   = false
  memory   = 4096
  restart  = "unless-stopped"
  shm_size = 256
  
  dynamic "ports" {
    for_each = local.ports
    content {
      internal = ports.value
      external = ports.value
    }
  }
  ports {
    internal = 80
    external = 8000
  }
  
  dynamic "volumes" {
  for_each = local.path_volume
      content {
      container_path = volumes.value.path
      volume_name = volumes.value.name
      }
  }
  
  lifecycle {
  ignore_changes = [image, name, memory_swap]
  }

}


